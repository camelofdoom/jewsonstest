﻿using System.Web.Mvc;

namespace JewsonsWeb.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}
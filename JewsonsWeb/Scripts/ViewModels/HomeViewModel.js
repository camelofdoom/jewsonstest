﻿function HomeViewModel() {
    var self = this;

    var xhr;

    self.loading = ko.observable(false);
    self.branch = ko.observable(new BranchViewModel());
    self.number = ko.observable();
    self.searchRun = ko.observable(false);
    self.results = ko.observable(false);

    self.search = function () {
        self.searchRun(true);
        self.loading(true);

        if (xhr) {
            xhr.abort();
        }

        //Url should be configurable
        xhr = $.get("http://localhost:62312/api/branches/" + self.number())
            .done(function (data) {
                self.number("");
                self.branch(new BranchViewModel(data));
                self.results(true);
                self.loading(false);
            }).error(function () {
                self.number("");
                self.results(false);
                self.loading(false);
            });
    };
}
﻿function BranchViewModel(data) {
    var self = this;

    data = data || {}

    ko.mapping.fromJS(data, {}, self);
}
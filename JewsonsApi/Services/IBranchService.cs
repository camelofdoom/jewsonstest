﻿namespace JewsonsApi.Services
{
	using Models;

	public interface IBranchService
	{
		Branch GetByNumber( int number );
	}
}

﻿namespace JewsonsApi.Services
{
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using Models;
	using Newtonsoft.Json;

	public class BranchService : IBranchService
	{
		private ICollection<Branch> Branches { get; }

		public BranchService()
		{
			var assembly = Assembly.GetExecutingAssembly();
			var resourceName = "JewsonsApi.Data.branches.json";

			string json = "";
			using (Stream stream = assembly.GetManifestResourceStream( resourceName ))
			{
				if (stream != null)
				{
					using(StreamReader reader = new StreamReader(stream))
					{
						json = reader.ReadToEnd();
					}
				}
			}

			Branches = JsonConvert.DeserializeObject<ICollection<Branch>>(json);
		}

		public Branch GetByNumber( int number )
		{
			return Branches.FirstOrDefault( p => p.Number == number );
		}
	}
}
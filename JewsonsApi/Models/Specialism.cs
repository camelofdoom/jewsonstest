﻿namespace JewsonsApi.Models
{
	public class Specialism
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }
	}
}
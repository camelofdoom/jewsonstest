﻿using System.Web.Http;

namespace JewsonsApi.Controllers
{
	using Services;

	[RoutePrefix("api/branches")]
	public class BranchController : ApiController
	{
		private readonly IBranchService service;

		public BranchController(IBranchService service)
		{
			this.service = service;
		}

		[HttpGet]
		[Route("{number:int}")]
		public IHttpActionResult Get(int number)
		{
			var branch = service.GetByNumber(number);

			if(branch == null)
			{
				return NotFound();
			}

			return Ok(branch);
		}
	}
}

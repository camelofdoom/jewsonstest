This is a more complex example of the kind of thing you were asking for, which I built a little while back to teach some of my juniors the basics of Ajax/KnockoutJs.
https://github.com/camelofcode/AjaxAndKnockout

This has more complex queries with paging/sorting/free text search, as well as interacting with query string so the query can be shared across clients.

You may also want to look at something I've done where I've had more time to make it look more appealing. This is a website I have just completed (it doesn't have a DNS entry yet) http://178.62.38.36/
Unlike the other examples this is using Angular on the front end - it is in fact MEAN stack (mongo, express, angular, node). There's some fancier stuff you can't see on the admin side of the site - but I opensourced a basic version of this site a little bit ago, so you can have a look at that here: https://github.com/camelofcode/MeanStackSkeleton